package com.demo.expclosect

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

/**
 * This activity will be called back when user clicks on a link which BrowserTabActivity is listening for.
 * As declared in the manifest
 *
 *  <activity android:name=".BrowserTabActivity" android:label="Browser Tab">
<intent-filter>
<action android:name="android.intent.action.VIEW" />
<category android:name="android.intent.category.BROWSABLE" />
<category android:name="android.intent.category.DEFAULT" />
<data  android:scheme="webauthtestapp" android:host="auth-success"/>
</intent-filter>
</activity>

 */

class BrowserTabActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_browser_tab)

        if (savedInstanceState == null) {

            Log.d("BrowserTabActivity", "" + intent?.dataString)

            Toast.makeText(this, "Closing Custom Tab ${intent?.dataString}", Toast.LENGTH_LONG).show()

            // Opening First MainActivity with given flags will clear the top of the stack.
            // Therefore custom tab (opened webpage) will be closed.
            // MainActivity is shown again.

            val myIntent = Intent(this, MainActivity::class.java)
            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(myIntent)

            finish()
        }

    }

}