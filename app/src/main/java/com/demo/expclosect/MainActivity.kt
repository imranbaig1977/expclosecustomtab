package com.demo.expclosect

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.browser.customtabs.CustomTabsIntent

class MainActivity : AppCompatActivity() {

    lateinit var logTextView : TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var button = findViewById<Button>(R.id.buttonRegister)
        var editText = findViewById<EditText>(R.id.editTextTextEmailAddress)

        logTextView = findViewById<TextView>(R.id.textView3)

        button.setOnClickListener {

            var customTabsIntent = CustomTabsIntent.Builder().build()

            customTabsIntent.launchUrl(this,
                    Uri.parse(editText.editableText.toString()))

            logTextView.text = "${logTextView.text}\nOpening ${editText.editableText.toString()}"
        }


    }
}